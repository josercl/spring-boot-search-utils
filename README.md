# How to use

## Gradle
```groovy
repositories {
    maven {
        url 'https://gitlab.com/api/v4/projects/52495705/packages/maven'
    }
}

dependencies {
    implementation 'com.gitlab.josercl:spring-boot-search-utils:1.0'
}
```

## Maven

Add Repo
```xml
<repositories>
    <repository>
        <id>repo-id</id>
        <url>https://gitlab.com/api/v4/projects/52495705/packages/maven</url>
    </repository>
</repositories>
```
Add dependency
```xml
<dependency>
    <groupId>com.gitlab.josercl</groupId>
    <artifactId>spring-boot-search-utils</artifactId>
    <version>1.0</version>
</dependency>
```

# Search
## Extend SearchCriteria

```java
public class SomeEntitySearchCriteria extends SearchCriteria<SomeEntity>{
    @SearchFilter(target = "entityField1", operator = SearchOperator.EQ)
    private String searchField1;

    @SearchFilter(target = "entityField2", operator = SearchOperator.GT)
    private Int searchField2;

    @SearchFilter(target = "entityField3", operator = SearchOperator.GTE)    
    private LocalDate searchField3;
    
    // Getters and setters
}
```

## Execute search

```java
var criteria = new SomeEntitySearchCriteria();
criteria.setSearchField1("xxxx");
criteria.setSearchField3(LocalDateTime.now());

Page<SomeEntity> = myEntityRepository.findAll(criteria, PageRequest.of(page, pageSize))
```
