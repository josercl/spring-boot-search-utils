package com.gitlab.josercl.search;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.domain.Specification;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Data
@Slf4j
public abstract class SearchCriteria<Entity> implements Specification<Entity> {
    protected Integer page;
    protected Integer pageSize;

    @Override
    public Predicate toPredicate(Root<Entity> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        return criteriaBuilder.and(getSearchPredicates(root, query, criteriaBuilder).toArray(Predicate[]::new));
    }

    public List<Predicate> getSearchPredicates(Root<Entity> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        return Arrays.stream(getClass().getDeclaredFields())
            .filter(field -> field.isAnnotationPresent(SearchFilter.class))
                .map(field -> {
                    field.setAccessible(true);
                    try {
                        Object value = field.get(this);
                        if (value == null) {
                            return null;
                        }
                        SearchFilter filter = field.getAnnotation(SearchFilter.class);
                        SearchStrategy strategy = switch(filter.operator()) {
                            case EQ -> new SearchStrategy.EQ(filter, value);
                            case GT -> new SearchStrategy.GT<>(filter,(Comparable) value);
                            case LT -> new SearchStrategy.LT<>(filter, (Comparable) value);
                            case GTE -> new SearchStrategy.GTE<>(filter,(Comparable) value);
                            case LTE -> new SearchStrategy.LTE<>(filter, (Comparable) value);
                            case IN -> new SearchStrategy.IN(filter, value);
                            case IEQ -> new SearchStrategy.IEQ(filter, (String) value);
                            case LIKE -> new SearchStrategy.LIKE(filter, (String) value);
                            case ILIKE -> new SearchStrategy.ILIKE(filter, (String) value);
                        };
                        return strategy.toPredicate(root, criteriaBuilder);
                    } catch (IllegalAccessException e) {
                        log.error(e.getLocalizedMessage(), e);
                    }
                    return null;
                })
                .filter(Objects::nonNull)
                .toList();
    }
}
