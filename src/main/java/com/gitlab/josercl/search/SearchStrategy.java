package com.gitlab.josercl.search;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public abstract class SearchStrategy<T> {
    protected final SearchFilter filter;
    protected final T value;

    Path<T> getPath(Root<T> root) {
        return root.get(filter.target());
    }

    public abstract Predicate toPredicate(Root<T> root, CriteriaBuilder criteriaBuilder);

    public static class EQ extends SearchStrategy<Object> {
        public EQ(SearchFilter filter, Object value) {
            super(filter, value);
        }

        @Override
        public Predicate toPredicate(Root<Object> root, CriteriaBuilder criteriaBuilder) {
            return criteriaBuilder.equal(getPath(root), value);
        }
    }

    public static class IEQ extends SearchStrategy<String> {
        public IEQ(SearchFilter filter, String value) {
            super(filter, value);
        }

        @Override
        public Predicate toPredicate(Root<String> root, CriteriaBuilder criteriaBuilder) {
            return criteriaBuilder.equal(criteriaBuilder.lower(getPath(root)), value.toLowerCase());
        }
    }

    public static class LIKE extends SearchStrategy<String> {
        public LIKE(SearchFilter filter, String value) {
            super(filter, value);
        }

        @Override
        public Predicate toPredicate(Root<String> root, CriteriaBuilder criteriaBuilder) {
            return criteriaBuilder.like(getPath(root), "%" + value + "%");
        }
    }

    public static class ILIKE extends SearchStrategy<String> {
        public ILIKE(SearchFilter filter, String value) {
            super(filter, value);
        }

        @Override
        public Predicate toPredicate(Root<String> root, CriteriaBuilder criteriaBuilder) {
            return criteriaBuilder.like(
                    criteriaBuilder.lower(getPath(root)),
                    "%" + value.toLowerCase() + "%"
            );
        }
    }

    public static class GT<T extends Comparable<? super T>> extends SearchStrategy<T> {
        public GT(SearchFilter filter, T value) {
            super(filter, value);
        }

        @Override
        public Predicate toPredicate(Root<T> root, CriteriaBuilder criteriaBuilder) {
            return criteriaBuilder.greaterThan(getPath(root), value);
        }
    }

    public static class LT<T extends Comparable<? super T>> extends SearchStrategy<T> {
        public LT(SearchFilter filter, T value) {
            super(filter, value);
        }

        @Override
        public Predicate toPredicate(Root<T> root, CriteriaBuilder criteriaBuilder) {
            return criteriaBuilder.lessThan(getPath(root), value);
        }
    }

    public static class GTE<T extends Comparable<? super T>> extends SearchStrategy<T> {
        public GTE(SearchFilter filter, T value) {
            super(filter, value);
        }

        @Override
        public Predicate toPredicate(Root<T> root, CriteriaBuilder criteriaBuilder) {
            return criteriaBuilder.greaterThanOrEqualTo(getPath(root), value);
        }
    }

    public static class LTE<T extends Comparable<? super T>> extends SearchStrategy<T> {
        public LTE(SearchFilter filter, T value) {
            super(filter, value);
        }

        @Override
        public Predicate toPredicate(Root<T> root, CriteriaBuilder criteriaBuilder) {
            return criteriaBuilder.lessThanOrEqualTo(getPath(root), value);
        }
    }

    public static class IN extends SearchStrategy<Object> {
        public IN(SearchFilter filter, Object value) {
            super(filter, value);
        }

        @Override
        public Predicate toPredicate(Root<Object> root, CriteriaBuilder criteriaBuilder) {
            if (!value.getClass().isArray()) {
                throw new IllegalArgumentException("IN operator must be used with arrays");
            }

            CriteriaBuilder.In<Object> in = criteriaBuilder.in(getPath(root));
            for(Object v: (Object[]) value) {
                in.value(v);
            }
            return in;
        }
    }
}
