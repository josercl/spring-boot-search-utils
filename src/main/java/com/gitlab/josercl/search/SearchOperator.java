package com.gitlab.josercl.search;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum SearchOperator {
    EQ, GT, LT, GTE, LTE, IN,
    /** Insensitive-case equals */ IEQ,
    /** Insensitive-case like */ LIKE, ILIKE
}
